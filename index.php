<?php

/**
 * Program nilai Ujian.
 * Copyright (c) 2018 Musa Sutisna
 * PondokIT Licensed
 */

class NilaiUjian {
	/* Manempung seluruh data peserta ujian
	 * dengan format 'Nama Peserta' => 'Nilai Peserta'.
	 */
	private $peserta = [];

	function __construct()
	{
		// memulai program
		$this->mulai();
		// memproses input user
		$this->loop();
		// mengakhiri program
		$this->akhiri();
	}

	/**
	 * Memulai menjalankan program.
	 *
	 * @return	void
	 */
	private function mulai()
	{
		echo "==>> Selamat datang di Program Input Nilai Ujian <<==\n";
	}

	/**
	 * Meminta user untuk memasukan input peserta ujian
	 * lalu inputan akan di tampung, input akan di lakukan secara berkala
	 * sampai user menyatakan pernyataan n (No).
	 *
	 * @return	void
	 */
	private function loop()
	{
		$loop = TRUE;

		while ($loop)
		{
			$nama = trim($this->inputNama());
			$nilai = trim($this->inputNilai($nama));
			$simpan = $this->simpanPeserta($nama, $nilai);

			echo 'Lanjutkan? [Y/n] : ';

			$lanjut = $this->getInput();

			if ($lanjut != "\n")
			{
				$lanjut = strtolower(trim($lanjut));

				if ($lanjut == 'y' || $lanjut == 'yes')
				{
					$loop = TRUE;
				}
				else
				{
					$loop = FALSE;
				}
			}
		}
	}

	/**
	 * Membaca input dari user.
	 *
	 * @return	String
	 */
	private function getInput()
	{
		$handle = fopen('php://stdin', 'r');
		$line	= fgets($handle); fclose($handle);

		return $line;
	}

	/**
	 * Meminta input nama peserta ujian.
	 *
	 * @return	String		nama peserta ujian
	 */
	private function inputNama()
	{
		echo "Masukkan Nama Peserta : ";

		$nama = $this->getInput();

		return $nama;
	}

	/**
	 * Meminta input nilai peserta ujian.
	 *
	 * @param	String		nama peserta ujian
	 * @return	void
	 */
	private function inputNilai($nama)
	{
		echo "Masukkan Nilai $nama : ";

		$nilai = floatval($this->getInput());

		// user tidak dapat memasukan nilai lebih dari 10
		if ($nilai > 10.0) {
			$nilai = 10.0;
		}

		return $nilai;
	}

	/**
	 * Menyimpan data peserta ujian.
	 *
	 * @param	String		nama peserta ujian
	 * @param	String		nilai peserta ujian
	 * @return	void
	 */
	private function simpanPeserta($nama, $nilai)
	{
		$this->peserta[$nama] = $nilai;

		echo "Nilai $nama adalah $nilai";
		echo "\n";
	}

	/**
	 * Mengakhiri program.
	 *
	 * @return	void
	 */
	private function akhiri()
	{
		echo "Terimakasih sudah memasukkan nilai nilai peserta Ujian\n";
		echo "\n";
		echo str_repeat('-', 49) . '*' . str_repeat('-', 49);
		echo "\n";

		// urutkan peserta berdasarkan nama berdasarkan abjad
		$listPeserta = $this->peserta;

		ksort($listPeserta);

		// nilai peserta di kelompokan menjadi tidak lulus dan lulus
		$pesertaLulus		= [];
		$pesertaTidakLulus	= [];

		// tampilkan seluruh peserta
		foreach ($listPeserta as $nama => $nilai)
		{
			echo "$nama => $nilai\n";

			// masukan ke kelompok
			if ($nilai > 5)
			{
				$pesertaLulus[$nama] = $nilai;
			}
			else
			{
				$pesertaTidakLulus[$nama] = $nilai;
			}
		}

		// tampilkan peserta yang lulus di urutkan berdasarkan nilai tertinggi
		echo "===> Nilai Lulus <==\n";

		arsort($pesertaLulus);

		foreach ($pesertaLulus as $nama => $nilai)
		{
			echo "Nilai ujian $nama telah mencukupi. Capaian ". $this->persen($nilai) . "%\n";
		}

		// tampilkan peserta yang tidak lulus berdasarkan nilai terendah
		echo "===> Nilai Tidak Lulus <==\n";

		asort($pesertaTidakLulus);

		foreach ($pesertaTidakLulus as $nama => $nilai)
		{
			echo "Nilai ujian $nama tidak mencukupi. Capaian ". $this->persen($nilai) . "%\n";
		}

		// dapatkan nilai tertinggi dan terendah
		// kumpulkan peserta dengan nilai tertinggi dan terendah
		// tampilkan peserta dengan nilai tertinggi dan terendah
		reset($pesertaLulus);
		$nilaiTertinggi	= key($pesertaLulus);

		if ($nilaiTertinggi == NULL)
		{
			$nilaiTertinggi = 0;
		}
		else
		{
			$nilaiTertinggi = $pesertaLulus[$nilaiTertinggi];
		}

		$pesertaTertinggi = [];

		foreach ($pesertaLulus as $nama => $nilai)
		{
			if ($nilai >= $nilaiTertinggi)
			{
				$pesertaTertinggi[$nama] = $nilai;
			}
		}

		arsort($pesertaTertinggi);

		reset($pesertaTidakLulus);
		$nilaiTerendah	= key($pesertaTidakLulus);

		if ($nilaiTerendah == NULL)
		{
			$nilaiTerendah = 0;
		}
		else
		{
			$nilaiTerendah = $pesertaTidakLulus[$nilaiTerendah];
		}

		$pesertaTerendah = [];

		foreach ($pesertaTidakLulus as $nama => $nilai)
		{
			if ($nilai <= $nilaiTerendah)
			{
				$pesertaTerendah[$nama] = $nilai;
			}
		}

		asort($pesertaTerendah);

		echo "\n";

		foreach ($pesertaTertinggi as $nama => $nilai)
		{
			echo "Nilai TERTINGGI adalah " . $nama. " dengan nilai " . $nilai . "\n";
		}

		foreach ($pesertaTerendah as $nama => $nilai)
		{
			echo "Nilai TERENDAH adalah " . $nama . " dengan nilai " . $nilai . "\n";
		}

		echo "\n";
		echo "Program dibuat dengan bahasa PHP versi 7.2.10 oleh Musa Sutisna\n\n";
	}

	/**
	 * Mengubah suatu nilai ke persen.
	 *
	 * @param	Integer
	 * @return	Integer
	 */
	private function persen($nilai)
	{
		return $nilai * 10;
	}
}

$nilaiUjian = new NilaiUjian();